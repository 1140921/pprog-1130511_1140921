/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.model.Autor;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextField;

/**
 *
 * @author 1140921
 */
public class JanelaNovoAutorDialog extends JDialog {

    private JTextField instituicaoAfiliacao;

    private JTextField nomeAutor;
    private JFrame framePai;
   
    private JList listaCompleta;

    public JanelaNovoAutorDialog(JFrame framePai, JList listaCompleta) {

        super(framePai);

        this.framePai = framePai;
        this.listaCompleta = listaCompleta;
        
        setLayout(new GridLayout(0, 1));
        Container c = getContentPane();
        c.add(criarPainelNome());
        c.add(criarPainelAfiliacao());
        c.add(criarPainelBotoes());

        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(framePai);

        setVisible(true);

    }

    private JPanel criarPainelNome() {

        JPanel painelNome = new JPanel();
        painelNome.add(new JLabel("Nome"));
        nomeAutor = new JTextField(15);
        painelNome.add(nomeAutor);

        return painelNome;

    }

    private JPanel criarPainelAfiliacao() {

        JPanel painelInstituicaoAfiliacao = new JPanel();
        painelInstituicaoAfiliacao.add(new JLabel("Instituição Afiliação"));

        instituicaoAfiliacao = new JTextField(15);
        painelInstituicaoAfiliacao.add(instituicaoAfiliacao);

        return painelInstituicaoAfiliacao;

    }

    private JPanel criarPainelBotoes() {

        JPanel painelBotoes = new JPanel();
        JButton okBtn = criarBotaoOK();

        JButton cancelarBtn = criarBotaoCancelar();

        painelBotoes.add(okBtn);
        painelBotoes.add(cancelarBtn);
        return painelBotoes;

    }

    private JButton criarBotaoOK() {
        JButton okBtn = new JButton("OK");
        okBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                try {
                    Autor autor = new Autor(nomeAutor.getText(), instituicaoAfiliacao.getText());
                    
                    
                    
                    ModeloListaAutores modeloListaAutores = (ModeloListaAutores) listaCompleta.getModel();
                    boolean autorAdicionadoArtigo = modeloListaAutores.addElement(autor);

                    dispose();
                    if (!autorAdicionadoArtigo) {
                        JOptionPane.showMessageDialog(framePai, "Autor já existente!", "Novo Autor", JOptionPane.ERROR_MESSAGE);
                    }
                } catch (IllegalArgumentException ias) {
                    JOptionPane.showMessageDialog(JanelaNovoAutorDialog.this, ias.getMessage(), "Novo Autor", JOptionPane.ERROR_MESSAGE);
                }

            }
        });
        return okBtn;
    }

    private JButton criarBotaoCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
        return btn;
    }

}
