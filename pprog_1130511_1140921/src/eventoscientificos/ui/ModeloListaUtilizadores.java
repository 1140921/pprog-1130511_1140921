/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.model.Evento;
import eventoscientificos.model.Utilizador;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;

/**
 *
 * @author 1140921
 */
public class ModeloListaUtilizadores extends AbstractTableModel {

    private static String[] nomeColunas = {"Nome", "Username", "Email"};
    private List<Utilizador> listaUtil;

    public ModeloListaUtilizadores() {
        this.listaUtil = new ArrayList<>();

    }

    @Override
    public int getRowCount() {
        return this.listaUtil.size();
    }
    
    @Override
    public int getColumnCount() {
        return ModeloListaUtilizadores.nomeColunas.length;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public String getColumnName(int colunaIndex) {
        return nomeColunas[colunaIndex];
    }

    @Override
    public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        String nomeColuna = getColumnName(columnIndex);
        String s;

        if (nomeColuna.equalsIgnoreCase(ModeloListaUtilizadores.nomeColunas[0])) {
            s = this.listaUtil.get(rowIndex).getNome();
        } else if (nomeColuna.equalsIgnoreCase(ModeloListaUtilizadores.nomeColunas[1])) {
            s = this.listaUtil.get(rowIndex).getUsername();

        } else {
            s = this.listaUtil.get(rowIndex).get_strEmail();
        }

        return s;
    }

    @Override
    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        String nomeColuna = getColumnName(columnIndex);

        if (nomeColuna.equalsIgnoreCase(ModeloListaUtilizadores.nomeColunas[0])) {
            this.listaUtil.get(rowIndex).setNome((String) value);
        } else if (nomeColuna.equalsIgnoreCase(ModeloListaUtilizadores.nomeColunas[1])) {
            this.listaUtil.get(rowIndex).setUsername((String) value);
        } else if (nomeColuna.equalsIgnoreCase(ModeloListaUtilizadores.nomeColunas[2])) {

        } else {
            this.listaUtil.get(rowIndex).setUsername((String) value);
        }

        fireTableCellUpdated(rowIndex, columnIndex);
    }

    public void addRow(Utilizador utilizador) {
        int rowCount = getRowCount();
        this.listaUtil.add(utilizador);
        fireTableRowsInserted(rowCount, rowCount);
    }

    public void removeRow(Utilizador utilizador) {
        int row = this.listaUtil.indexOf(utilizador);
        this.listaUtil.remove(utilizador);
        fireTableRowsDeleted(row, row);
    }

    public Evento[] getArray() {
       return this.listaUtil.toArray(new Evento[this.listaUtil.size()]);
    }

}
