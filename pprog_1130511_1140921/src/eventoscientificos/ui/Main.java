package eventoscientificos.ui;

import eventoscientificos.model.*;
import java.io.*;
import java.util.*;
import javax.swing.*;

/**
 *
 * @author 1140921
 */
public class Main {

    public static void main(String[] args) throws FileNotFoundException {
        if (args.length == 1) {

            String FILE_TXT = args[0];

            Empresa empresa = new Empresa();

            ModeloListaUtilizadores modeloTabelaUtilizador = new ModeloListaUtilizadores();
            for (Utilizador u : FicheiroAInstanciarTxt.lerFicheiroTxtUtilizador(FILE_TXT)) {
                empresa.addUtilizador(u);
                modeloTabelaUtilizador.addRow(u);
            }

            JTable tableListaUtilizadores = new JTable(modeloTabelaUtilizador);

            ModeloListaEventos modeloListaEventos = new ModeloListaEventos();

            FicheiroInformacaoBin ficheiroEvento = new FicheiroInformacaoBin();

            ArrayList<Evento> listaEventos = ficheiroEvento.ler(FicheiroInformacaoBin.NOME);

            if (listaEventos == null) {
                listaEventos = FicheiroAInstanciarTxt.lerFicheiroTxtEventos(FILE_TXT);
            }

            for (Evento s : listaEventos) {
                empresa.addEvento(s);
                modeloListaEventos.addRow(s);
            }

            JTable tableListaEventos = new JTable(modeloListaEventos);

            new Janela("Submeter", tableListaUtilizadores, tableListaEventos, empresa, ficheiroEvento);

        }
    }

}
