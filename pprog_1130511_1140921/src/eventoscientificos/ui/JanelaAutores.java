/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.model.Artigo;
import eventoscientificos.model.Autor;
import java.awt.BorderLayout;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.util.ArrayList;
import java.util.List;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.KeyStroke;
import javax.swing.ListSelectionModel;
import javax.swing.UIManager;
import javax.swing.event.ListDataEvent;
import javax.swing.event.ListDataListener;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author 1140921
 */
public class JanelaAutores extends JDialog {

    private JList lstCompleta, lstCorrespondente;
    private JButton btnEliminarCompleta, btnEliminarCorrespondente;
    private JButton btnPromoverAutor, btnSubmissao;
    private ModeloListaAutores modeloListaCompleta;
    private ModeloListaAutores modeloListaCorrespondentes;
    private JMenuItem menuItemGuardar;
    private JFrame framePai;
    private Artigo artigo;
    private List<Autor> listaAutores;
    private List<Autor> autorCorrespondente;

    public JanelaAutores(JFrame framePai, Artigo artigo) {

        super();
        this.framePai = framePai;
        this.artigo = artigo;
        setLayout(new GridLayout(0, 1));
        JMenuBar menuBar = new JMenuBar();
        setJMenuBar(menuBar);
        menuBar.add(criarMenu());

        this.listaAutores = new ArrayList<Autor>();
        this.autorCorrespondente = new ArrayList<Autor>();

        add(criarPainelListas());

        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        setResizable(false);
        setLocationRelativeTo(framePai);
        setVisible(true);
    }

    private JMenu criarMenu() {

        JMenu menu = new JMenu("Menu");
        menu.setMnemonic(KeyEvent.VK_F);

        menu.add(criarItemNovoAutor());

        menu.addSeparator();


      
        menuItemGuardar = criarItemGuardarPdf();
        menu.add(menuItemGuardar);

        personalizarFileChooserEmPortugues();

        menu.addSeparator();

        menu.add(criarItemSair());

        return menu;
    }

    private JMenuItem criarItemNovoAutor() {
        JMenuItem itemAutor = new JMenuItem("Novo Autor", KeyEvent.VK_N);
        itemAutor.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_MASK));

        itemAutor.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                new JanelaNovoAutorDialog(framePai, lstCompleta);
            }

        });

        return itemAutor;
    }

    private JMenuItem criarItemSair() {
        JMenuItem item = new JMenuItem("Sair", KeyEvent.VK_S);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                sair();
            }
        });
        return item;
    }

    private JPanel criarPainelListas() {
        final int NUMERO_LINHAS = 1, NUMERO_COLUNAS = 2;
        final int INTERVALO_HORIZONTAL = 20, INTERVALO_VERTICAL = 0;
        JPanel p = new JPanel(new GridLayout(NUMERO_LINHAS, NUMERO_COLUNAS, INTERVALO_HORIZONTAL, INTERVALO_VERTICAL));

        modeloListaCompleta = new ModeloListaAutores(listaAutores);
        lstCompleta = new JList(modeloListaCompleta);
        btnEliminarCompleta = criarBotaoEliminarAutor(lstCompleta);
        btnPromoverAutor = criarBotaoPromoverlAutorCorrespondente();

        modeloListaCorrespondentes = new ModeloListaAutores(autorCorrespondente);
        lstCorrespondente = new JList(modeloListaCorrespondentes);
        btnEliminarCorrespondente = criarBotaoEliminarAutor(lstCorrespondente);
        btnSubmissao = criarBotaoCriarSubmissao();
        p.add(criarPainelLista("Lista de Autores", lstCompleta, lstCorrespondente, modeloListaCompleta, modeloListaCorrespondentes, btnEliminarCompleta, btnPromoverAutor));
        p.add(criarPainelListaCorrespondente("Lista Autores correspondentes", lstCorrespondente, modeloListaCorrespondentes, btnEliminarCorrespondente, btnSubmissao));
        return p;
    }

    private JPanel criarPainelLista(String tituloLista, JList lstLista, JList lstCorrespondente, ModeloListaAutores modeloLista, ModeloListaAutores modeloListaCorrespondente, JButton btnSuperior, JButton btnInferior) {
        JLabel lblTitulo = new JLabel(tituloLista, JLabel.LEFT);

        modeloLista.addListDataListener(new ListDataListener() {
            @Override
            public void intervalAdded(ListDataEvent e) {
                if (lstCorrespondente.getModel().getSize() == 0) {
                    btnInferior.setEnabled(lstLista.getModel().getSize() != 0);
                } else {
                    btnInferior.setEnabled(false);
                }
                btnSuperior.setEnabled(lstLista.getModel().getSize() != 0);

            }

            @Override
            public void intervalRemoved(ListDataEvent e) {
                btnSuperior.setEnabled(lstLista.getModel().getSize() != 0);
                btnInferior.setEnabled(lstLista.getModel().getSize() != 0);

            }

            @Override
            public void contentsChanged(ListDataEvent e) {
            }
        });
        modeloListaCorrespondente.addListDataListener(new ListDataListener() {

            @Override
            public void intervalAdded(ListDataEvent e) {
                if (lstCorrespondente.getModel().getSize() == 1) {
                    btnInferior.setEnabled(false);
                }

            }

            @Override
            public void intervalRemoved(ListDataEvent e) {
                btnInferior.setEnabled(lstLista.getModel().getSize() != 0);
            }

            @Override
            public void contentsChanged(ListDataEvent e) {

            }
        });

        lstLista.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane scrPane = new JScrollPane(lstLista);

        JPanel p = new JPanel(new BorderLayout());

        final int MARGEM_SUPERIOR = 20, MARGEM_INFERIOR = 20;
        final int MARGEM_ESQUERDA = 20, MARGEM_DIREITA = 20;
        p.setBorder(BorderFactory.createEmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA, MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lblTitulo, BorderLayout.NORTH);
        p.add(scrPane, BorderLayout.CENTER);

        JPanel pBotoes = criarPainelBotoes(btnSuperior, btnInferior);
        p.add(pBotoes, BorderLayout.SOUTH);
        return p;
    }

    private JPanel criarPainelListaCorrespondente(String tituloLista, JList lstLista, ModeloListaAutores modeloLista, JButton btnSuperior, JButton btnInferior) {
        JLabel lblTitulo = new JLabel(tituloLista, JLabel.LEFT);

        modeloLista.addListDataListener(new ListDataListener() {
            @Override
            public void intervalAdded(ListDataEvent e) {
                btnSuperior.setEnabled(lstLista.getModel().getSize() != 0);
                btnInferior.setEnabled(lstLista.getModel().getSize() != 0);

            }

            @Override
            public void intervalRemoved(ListDataEvent e) {
                btnSuperior.setEnabled(lstLista.getModel().getSize() != 0);
                btnInferior.setEnabled(lstLista.getModel().getSize() != 0);
            }

            @Override
            public void contentsChanged(ListDataEvent e) {
            }
        });

        lstLista.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        JScrollPane scrPane = new JScrollPane(lstLista);

        JPanel p = new JPanel(new BorderLayout());

        final int MARGEM_SUPERIOR = 20, MARGEM_INFERIOR = 20;
        final int MARGEM_ESQUERDA = 20, MARGEM_DIREITA = 20;
        p.setBorder(BorderFactory.createEmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA, MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(lblTitulo, BorderLayout.NORTH);
        p.add(scrPane, BorderLayout.CENTER);

        JPanel pBotoes = criarPainelBotoes(btnSuperior, btnInferior);
        p.add(pBotoes, BorderLayout.SOUTH);
        return p;
    }

    private JPanel criarPainelBotoes(JButton btn1, JButton btn2) {

        final int NUMERO_LINHAS = 2, NUMERO_COLUNAS = 1;
        final int INTERVALO_HORIZONTAL = 0, INTERVALO_VERTICAL = 10;
        JPanel p = new JPanel(new GridLayout(NUMERO_LINHAS, NUMERO_COLUNAS, INTERVALO_HORIZONTAL, INTERVALO_VERTICAL));

        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 0;
        final int MARGEM_ESQUERDA = 0, MARGEM_DIREITA = 0;
        p.setBorder(BorderFactory.createEmptyBorder(MARGEM_SUPERIOR, MARGEM_ESQUERDA, MARGEM_INFERIOR, MARGEM_DIREITA));

        p.add(btn1);
        p.add(btn2);

        return p;
    }

    private JButton criarBotaoEliminarAutor(JList lstLista) {
        JButton eliminarBtn = new JButton("Eliminar Autor");
        if (lstLista == lstCompleta) {
            btnEliminarCompleta = eliminarBtn;
            btnEliminarCompleta.setEnabled(lstCompleta.getModel().getSize() != 0);
        } else {
            btnEliminarCorrespondente = eliminarBtn;
            btnEliminarCorrespondente.setEnabled(lstCorrespondente.getModel().getSize() != 0);
        }
        eliminarBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                Autor autorSelecionado = (Autor) lstLista.getSelectedValue();
                if (autorSelecionado == null) {
                    JOptionPane.showMessageDialog(JanelaAutores.this,
                            "Seleccione um autor.",
                            "Eliminar autor",
                            JOptionPane.WARNING_MESSAGE);
                } else {
                    String[] itens = {"Sim", "Não"};
                    int resposta = JOptionPane.showOptionDialog(JanelaAutores.this,
                            "Eliminar\n" + autorSelecionado.toString(),
                            "Eliminar autor",
                            0,
                            JOptionPane.QUESTION_MESSAGE,
                            null,
                            itens,
                            itens[1]);
                    final int SIM = 0;
                    if (resposta == SIM) {
                        ModeloListaAutores modeloLista
                                = (ModeloListaAutores) lstLista.getModel();
                        modeloLista.removeElement(autorSelecionado);
                        if (lstLista == lstCompleta) {
                            ModeloListaAutores modeloListacorrespondentes
                                    = (ModeloListaAutores) lstCorrespondente.getModel();
                            if (modeloListacorrespondentes.getSize() != 0
                                    && modeloListacorrespondentes.contains(autorSelecionado)) {
                                modeloListacorrespondentes.removeElement(autorSelecionado);
                            }
                        }
                    }
                }
            }
        });
        return eliminarBtn;
    }

    private JButton criarBotaoPromoverlAutorCorrespondente() {
        btnPromoverAutor = new JButton("Promover autor correspondente");

        btnPromoverAutor.setEnabled(lstCompleta.getModel().getSize() != 0);
        btnPromoverAutor.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                ModeloListaAutores modeloListaCorrespondentes = (ModeloListaAutores) lstCorrespondente.getModel();
                Autor autorSelecionado = (Autor) lstCompleta.getSelectedValue();
                if (autorSelecionado == null) {
                    JOptionPane.showMessageDialog(JanelaAutores.this,
                            "Seleccione um autor.",
                            "Promover a autor correspondente",
                            JOptionPane.WARNING_MESSAGE);
                } else if (modeloListaCorrespondentes.contains(autorSelecionado)) {
                    JOptionPane.showMessageDialog(JanelaAutores.this,
                            "Autor já está promovido!",
                            "Promover autor ",
                            JOptionPane.WARNING_MESSAGE);
                } else {
                    String[] itens = {"Sim", "Não"};
                    int resposta = JOptionPane.showOptionDialog(JanelaAutores.this,
                            "Promover\n" + autorSelecionado.toString(),
                            "Promover autor a correspondente ?",
                            0,
                            JOptionPane.QUESTION_MESSAGE,
                            null,
                            itens,
                            itens[1]);
                    final int SIM = 0;
                    if (resposta == SIM) {
                        modeloListaCorrespondentes.addElement(autorSelecionado);
                    }
                }
            }
        });

        return btnPromoverAutor;
    }

    private JMenuItem criarItemGuardarPdf() {
        JMenuItem item = new JMenuItem("Inserir Ficheiro PDF", KeyEvent.VK_G);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_G, InputEvent.CTRL_MASK));

        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JFileChooser fileChooser = new JFileChooser();
                fileChooser = new JFileChooser();
                definirFiltroExtensaoPdf(fileChooser);
                int resposta = fileChooser.showOpenDialog(JanelaAutores.this);
                if (resposta == JFileChooser.APPROVE_OPTION) {

                    File file = fileChooser.getSelectedFile();
                    artigo.setFicheiro(file.getPath());

                    JOptionPane.showMessageDialog(JanelaAutores.this, "Ficheiro PDF submetido");

                }

            }
        });

        return item;

    }

    private JButton criarBotaoCriarSubmissao() {
        btnSubmissao = new JButton("Finalizar Submissão");

        btnSubmissao.setEnabled(lstCorrespondente.getModel().getSize() != 0);

        btnSubmissao.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    if (artigo.getStrFicheiro().equalsIgnoreCase("sem ficheiro")) {
                        throw new NullPointerException("Ficheiro não Submetido");
                    }

                    artigo.setListaAutores(listaAutores);

                    for (Autor a : autorCorrespondente) {

                        artigo.setAutorCorrespondente(a);
                    }
                    dispose();
                } catch (NullPointerException i) {
                    JOptionPane.showMessageDialog(JanelaAutores.this, i.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);

                }

            }
        }
        );

        return btnSubmissao;
    }

    private void sair() {

        dispose();
    }

    private void definirFiltroExtensaoPdf(JFileChooser fileChooser) {
        fileChooser.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                }
                String extensao = extensao(f);
                if (extensao != null) {
                    return extensao.equals("pdf");
                }
                return false;
            }

            @Override
            public String getDescription() {
                return "*.pdf";
            }

            private String extensao(File f) {
                String ext = null;
                String s = f.getName();
                int i = s.lastIndexOf(".");
                if (i != -1) {
                    ext = s.substring(i + 1).toLowerCase();
                }
                return ext;
            }
        });
    }

    public static void personalizarFileChooserEmPortugues() {

        UIManager.put("FileChooser.openDialogTitleText", "Abrir");
        UIManager.put("FileChooser.saveDialogTitleText", "Guardar");

        UIManager.put("FileChooser.acceptAllFileFilterText", "Todos os Ficheiros");

        UIManager.put("FileChooser.lookInLabelMnemonic", "E");
        UIManager.put("FileChooser.lookInLabelText", "Procurar em:");

        UIManager.put("FileChooser.saveInLabelMnemonic", "S");
        UIManager.put("FileChooser.saveInLabelText", "Guardar em:");

        UIManager.put("FileChooser.upFolderToolTipText", "Um nível acima");
        UIManager.put("FileChooser.upFolderAccessibleName", "Um nível acima");

        UIManager.put("FileChooser.homeFolderToolTipText", "Ambiente de Trabalho");
        UIManager.put("FileChooser.homeFolderAccessibleName", "Ambiente de Trabalho");

        UIManager.put("FileChooser.newFolderToolTipText", "Criar nova pasta");
        UIManager.put("FileChooser.newFolderAccessibleName", "Criar nova pasta");

        UIManager.put("FileChooser.listViewButtonToolTipText", "Lista");
        UIManager.put("FileChooser.listViewButtonAccessibleName", "Lista");

        UIManager.put("FileChooser.detailsViewButtonToolTipText", "Detalhes");
        UIManager.put("FileChooser.detailsViewButtonAccessibleName", "Detalhes");

        UIManager.put("FileChooser.fileNameLabelMnemonic", "N");
        UIManager.put("FileChooser.fileNameLabelText", "Nome do ficheiro:");

        UIManager.put("FileChooser.filesOfTypeLabelMnemonic", "A");
        UIManager.put("FileChooser.filesOfTypeLabelText", "Ficheiros do tipo:");

        UIManager.put("FileChooser.fileNameHeaderText", "Nome");
        UIManager.put("FileChooser.fileSizeHeaderText", "Tamanho");
        UIManager.put("FileChooser.fileTypeHeaderText", "Tipo");
        UIManager.put("FileChooser.fileDateHeaderText", "Data");
        UIManager.put("FileChooser.fileAttrHeaderText", "Atributos");

        UIManager.put("FileChooser.cancelButtonText", "Cancelar");
        UIManager.put("FileChooser.cancelButtonMnemonic", "C");
        UIManager.put("FileChooser.cancelButtonToolTipText", "Cancelar");

        UIManager.put("FileChooser.openButtonText", "Abrir");
        UIManager.put("FileChooser.openButtonMnemonic", "A");
        UIManager.put("FileChooser.openButtonToolTipText", "Abrir");

        UIManager.put("FileChooser.saveButtonText", "Guardar");
        UIManager.put("FileChooser.saveButtonToolTipText", "S");
        UIManager.put("FileChooser.saveButtonToolTipText", "Guardar");

        UIManager.put("FileChooser.updateButtonText", "Alterar");
        UIManager.put("FileChooser.updateButtonToolTipText", "A");
        UIManager.put("FileChooser.updateButtonToolTipText", "Alterar");

        UIManager.put("FileChooser.helpButtonText", "Ajuda");
        UIManager.put("FileChooser.helpButtonToolTipText", "A");
        UIManager.put("FileChooser.helpButtonToolTipText", "Ajuda");
    }

}
