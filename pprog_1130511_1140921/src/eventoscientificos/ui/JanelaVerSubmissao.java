/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.model.Artigo;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Sessao;
import eventoscientificos.model.Submissivel;
import java.awt.PopupMenu;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JTextArea;

/**
 *
 * @author 1140921
 */
public class JanelaVerSubmissao extends JDialog {

    private JFrame jframePai;
    private Submissivel submissivel;
    private Artigo artigo;
    private JLabel lblEvento, lblArtigo, lblSessao;
    private JTextArea txtEvento, txtArtigo, txtSessao;

    public JanelaVerSubmissao(JFrame jframePai, Submissivel submissivel, Artigo artigo) {
        super();
         setLocationRelativeTo(jframePai);
        this.jframePai = jframePai;
        this.submissivel = submissivel;
        this.artigo = artigo;

      
       
        if (submissivel instanceof Evento) {
            add(criarPainelEvento());

        } else {
            add(criarPainelSessao());
        }
        
        pack();

        setModal(true);

        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(jframePai);
        setVisible(true);
    }

    private JPanel criarPainelEvento() {
        JPanel p = new JPanel();
        Evento evento = (Evento) this.submissivel;

        this.lblEvento = new JLabel("Evento");
        this.lblArtigo = new JLabel("Artigo");
        this.txtEvento = new JTextArea(evento.toString());
        this.txtArtigo = new JTextArea(artigo.toString());
        txtEvento.setEditable(false);
        txtArtigo.setEditable(false);
        p.add(lblEvento);
        p.add(txtEvento);
        p.add(txtEvento);
        p.add(txtArtigo);
        return p;

    }

    private JPanel criarPainelSessao() {
        JPanel p = new JPanel();
        Sessao sessao = (Sessao) this.submissivel;

        this.lblSessao = new JLabel("Sessão");
        this.lblArtigo = new JLabel("Artigo");
        this.txtEvento = new JTextArea(sessao.toString());
        this.txtArtigo = new JTextArea(artigo.toString());
        txtSessao.setEditable(false);
        txtArtigo.setEditable(false);
        p.add(lblSessao);
        p.add(txtEvento);
        p.add(txtEvento);
        p.add(txtArtigo);
        return p;

    }

}
