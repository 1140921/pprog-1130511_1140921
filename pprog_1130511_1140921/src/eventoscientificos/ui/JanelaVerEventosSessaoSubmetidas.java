/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.model.Artigo;
import eventoscientificos.model.Submissao;
import eventoscientificos.model.Submissivel;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author 1140921
 */
public class JanelaVerEventosSessaoSubmetidas extends JDialog {

    private Submissivel submissivel;
    private JComboBox<Submissao> listSubmissoes;
    private JFrame jframePai;

    public JanelaVerEventosSessaoSubmetidas(JFrame jframePai, Submissivel submissivel) {
        super();
        setLocationRelativeTo(jframePai);
        this.jframePai = jframePai;
        this.submissivel = submissivel;

        add(criarPainelEscolherSubmissao(), BorderLayout.CENTER);
        add(criarPainelButtons(), BorderLayout.SOUTH);

        pack();

        setModal(true);

        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        setLocationRelativeTo(jframePai);
        setVisible(true);
    }

    private JPanel criarPainelEscolherSubmissao() {

        JPanel p = new JPanel();
        listSubmissoes = new JComboBox<>();
        for (Submissao s : submissivel.getListaSubmissoes()) {
            listSubmissoes.addItem(s);
        }
        listSubmissoes.setSelectedItem(0);
        p.add(listSubmissoes);
        return p;

    }

    private JPanel criarPainelButtons() {
        JPanel p = new JPanel();
        p.add(criarButtonSelecionar());
        p.add(criarButtonCancelar());
        return p;
    }

    private JButton criarButtonSelecionar() {
        JButton btn = new JButton("Selecionar");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try {

                    Submissao submissaoSelecionada = (Submissao) listSubmissoes.getSelectedItem();
                    
                    if (submissaoSelecionada == null) {
                        throw new NullPointerException("nenhuma submissão feita");
                    }
                    Artigo artigo = submissaoSelecionada.getArtigo();
                    new JanelaVerSubmissao(jframePai, submissivel, artigo);
                    dispose();

                } catch (NullPointerException i) {
                    JOptionPane.showMessageDialog(jframePai, i.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);

                }
              

            }
        });

        return btn;

    }

    private JButton criarButtonCancelar() {
        JButton btnCancelar = new JButton("Cancelar");
        btnCancelar.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                dispose();
            }
        });

        return btnCancelar;
    }
}
