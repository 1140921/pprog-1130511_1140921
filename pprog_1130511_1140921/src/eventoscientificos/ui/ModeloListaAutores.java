/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.model.Autor;
import java.util.List;
import javax.swing.AbstractListModel;

/**
 *
 * @author 1140921
 */
public class ModeloListaAutores extends AbstractListModel {

    private List<Autor> listaAutores;

    public ModeloListaAutores(List<Autor> listaAutores) {

        this.listaAutores = listaAutores;
    }

    public ModeloListaAutores(Autor autorCorrespondente) {

    }

    @Override
    public int getSize() {
        if (this.listaAutores != null) {
            return this.listaAutores.size();
        } else {
            return 0;
        }
    }

    @Override
    public Object getElementAt(int indice) {
        return this.listaAutores.get(indice);
    }

    public boolean addElement(Autor autor) {
        boolean autorAdicionado = this.listaAutores.add(autor);
        if (autorAdicionado) {
            fireIntervalAdded(this, this.getSize() - 1, this.getSize() - 1);
        }
        return autorAdicionado;
    }

    public boolean removeElement(Autor autor) {
        int indice = this.listaAutores.indexOf(autor);
        boolean autorRemovido = this.listaAutores.remove(autor);
        if (autorRemovido) {
            fireIntervalRemoved(this, indice, indice);
        }
        return autorRemovido;
    }

    public boolean contains(Autor autor) {
        return this.listaAutores.contains(autor);
    }

}
