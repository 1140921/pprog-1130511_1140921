/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

//import eventoscientificos.model.Empresa;
import eventoscientificos.model.Artigo;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTextField;

/**
 *
 * @author 1140921
 */
public class SubmeterArtigoUI extends JDialog {

    private JTextField txtTituloArtigo;
    private JTextArea txtResumoArtigo;
    private Artigo artigo;
    private JFrame framePai;

    public SubmeterArtigoUI(JFrame framePai, Artigo artigo) {
        super();
        this.artigo = artigo;
        this.framePai=framePai;
        setLayout(new GridLayout(0, 1));
        Container c = getContentPane();
        c.add(criarPainelTitulo());
        c.add(criarPainelResumo());
        c.add(criarPainelBotoes());

        pack();
        setModal(true);
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);

        setResizable(false);
        setLocationRelativeTo(framePai);
        setVisible(true);
    }

    private JPanel criarPainelTitulo() {
        JPanel painelTitulo = new JPanel();
        painelTitulo.add(new JLabel("Titulo :"));
        txtTituloArtigo = new JTextField(15);
        painelTitulo.add(txtTituloArtigo);

        return painelTitulo;
    }

    private JPanel criarPainelResumo() {
        JPanel painelResumo = new JPanel();
        painelResumo.add(new JLabel("Resumo:"));
        txtResumoArtigo = new JTextArea(10, 30);
        txtResumoArtigo.setMaximumSize(txtResumoArtigo.getPreferredSize());
        JScrollPane jScrollPane = new JScrollPane(txtResumoArtigo);

        painelResumo.add(jScrollPane);
        painelResumo.add(txtResumoArtigo);

        return painelResumo;
    }

    private JPanel criarPainelBotoes() {

        JPanel painelBotoes = new JPanel();

        JButton submeterBtn = new JButton("Submeter");

        submeterBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                try {

                    artigo.setTitulo(txtTituloArtigo.getText());
                    artigo.setResumo(txtResumoArtigo.getText());
                  
                    dispose();

                    new JanelaAutores(framePai,artigo);
                } catch (IllegalArgumentException s) {
                    JOptionPane.showMessageDialog(SubmeterArtigoUI.this, s.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
                }

            }
        });

        JButton cancelBtn = new JButton("Cancelar");

        cancelBtn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                dispose();
            }
        });

        painelBotoes.add(submeterBtn);
        painelBotoes.add(cancelBtn);

        return painelBotoes;
    }
}
