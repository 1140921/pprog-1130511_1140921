/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

/**
 *
 * @author 1140921
 */
import eventoscientificos.model.Artigo;
import eventoscientificos.model.Evento;
import eventoscientificos.model.Sessao;
import eventoscientificos.model.Submissao;
import eventoscientificos.model.Submissivel;
import eventoscientificos.ui.SubmeterArtigoUI;
import java.awt.Container;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.event.ActionListener;
import javax.swing.JOptionPane;

public class JanelaSubmeterEventoSessoes extends JDialog {

    private JFrame jframePai;
    private Evento eventoSelecionado;
    private JComboBox<Submissivel> listSubmissivel;

    public JanelaSubmeterEventoSessoes(JFrame jframePai, Evento eventoSelecionado) {
         super(jframePai);
         setLayout(new GridLayout(0, 1));
        this.jframePai = jframePai;
        this.eventoSelecionado = eventoSelecionado;
        Container c = getContentPane();
        c.add(criarPainelSelecao());
        c.add(criarPainelButtons());
        
        pack();

        setModal(true);
       
        
        setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
       setLocationRelativeTo(jframePai);
       setVisible(true);
    }

    public JPanel criarPainelSelecao() {
        JPanel p = new JPanel();
        this.listSubmissivel = new JComboBox<>();
        listSubmissivel.addItem(eventoSelecionado);
        for (Sessao s : eventoSelecionado.getListaSessoes()) {
            if (s != null) {
                listSubmissivel.addItem(s);
            }
        }
        listSubmissivel.setSelectedItem(0);
        p.add(listSubmissivel);
        return p;
    }

    public JPanel criarPainelButtons() {
        JPanel p = new JPanel();
        p.add(criarButtonSelecionar());
        p.add(criarButtonCancelar());
        return p;
    }

    public JButton criarButtonSelecionar() {
        JButton btn = new JButton("Selecionar");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                try{
                Submissivel s = (Submissivel) listSubmissivel.getSelectedItem();
                if(s== null){
                    throw new IllegalArgumentException("Nenhum Evento/Sessao Temática selecionado");
                } 
                Artigo artigo = new Artigo();
                Submissao submissao = s.novaSubmissao(artigo);
                s.addSubmissao(submissao);
                dispose();
                new SubmeterArtigoUI(jframePai, artigo);
                }catch(IllegalArgumentException i){
                   JOptionPane.showMessageDialog(JanelaSubmeterEventoSessoes.this, i.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
                    
                }
               
               
            }
        });

        return btn;
    }

    public JButton criarButtonCancelar() {
        JButton btn = new JButton("Cancelar");
        btn.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });

        return btn;
    }

}
