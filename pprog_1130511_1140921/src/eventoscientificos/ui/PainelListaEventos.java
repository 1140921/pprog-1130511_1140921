/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import java.awt.BorderLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 *
 * @author 1140921
 */
public class PainelListaEventos extends JPanel {

    private JTable tabelaListaEventos;

    private JFrame jframePai;
    private Empresa empresa;
    

    public PainelListaEventos(JFrame jframePai, JTable tabelaListaEventos, Empresa empresa ) {
        super(new BorderLayout());
        this.jframePai = jframePai;

        this.tabelaListaEventos = tabelaListaEventos;
        this.empresa = empresa;
        final int SINGLE_ROW_SELECTION = 0;
        tabelaListaEventos.setSelectionMode(SINGLE_ROW_SELECTION);

        JPanel p = criarPainelListaEventos();
        JPanel p1 = new JPanel();
        JButton btn = criarBotao();
        JButton btn2 = criarBotaoVerEventoSessao();
        p1.add(btn);
        p1.add(btn2, BorderLayout.SOUTH);

        add(p, BorderLayout.CENTER);
        add(p1, BorderLayout.SOUTH);

    }

    public JTable getTableListaEventos() {
        return tabelaListaEventos;
    }

    private JPanel criarPainelListaEventos() {
        tabelaListaEventos.setAutoCreateRowSorter(true);
        JScrollPane scrollPane = new JScrollPane(tabelaListaEventos);
        JPanel p = new JPanel(new BorderLayout());
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(BorderFactory.createEmptyBorder(MARGEM_SUPERIOR,
                MARGEM_ESQUERDA,
                MARGEM_INFERIOR,
                MARGEM_DIREITA));
        p.add(scrollPane, BorderLayout.CENTER);
        return p;
    }

    public JButton criarBotao() {
        JButton btSubmeter = new JButton("Submissão");

        btSubmeter.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                int rowSelecionada = tabelaListaEventos.getSelectedRow();

                try {

                    if (rowSelecionada < 0) {
                        throw new NullPointerException("Nenhum Evento selecionado");
                    } else {
                        Evento evento = empresa.getListaEventosPodeSubmeter().get(rowSelecionada);

                        new JanelaSubmeterEventoSessoes(jframePai, evento);
                    }
                } catch (NullPointerException s) {
                    JOptionPane.showMessageDialog(PainelListaEventos.this, s.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
                }

            }
        });

        return btSubmeter;
    }

    private JButton criarBotaoVerEventoSessao( ) {
        JButton btnVerSubmissao = new JButton("Ver Submissão");

        btnVerSubmissao.addActionListener(new ActionListener() {

            @Override
            public void actionPerformed(ActionEvent e) {

                try {
                    int rowSelecionada = tabelaListaEventos.getSelectedRow();
                    if (rowSelecionada < 0) {
                        throw new NullPointerException("Nenhum Evento selecionado");
                    } else {

                        Evento evento = empresa.getListaEventosPodeSubmeter().get(rowSelecionada);
                      
                        
                        new JanelaVerEventosSessaoSubmetidas(jframePai, evento);
                    }
                } catch (NullPointerException s) {
                    JOptionPane.showMessageDialog(jframePai, s.getMessage(), "Erro", JOptionPane.ERROR_MESSAGE);
                }

            }
        });

     

        return btnVerSubmissao;
    }

}
