/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.model.Empresa;
import eventoscientificos.model.Evento;
import eventoscientificos.model.FicheiroAInstanciarTxt;
import eventoscientificos.model.FicheiroInformacaoBin;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.InputEvent;
import java.awt.event.KeyEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.util.ArrayList;
import javax.swing.JFileChooser;
import javax.swing.JFrame;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JOptionPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.KeyStroke;
import javax.swing.UIManager;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author 1140921
 */
public class Janela extends JFrame {

    private JTabbedPane tabPane;
    private JTable tabelaListaUtilizadores, tabelaListaEventos;
    private JFileChooser fileChooser;
    private Empresa empresa;
    private FicheiroInformacaoBin ficheiroInformacaoBinEvento;

    public Janela(String titulo, JTable tabelaListaUtilizadores, JTable tabelaListaEventos, Empresa empresa,FicheiroInformacaoBin ficheiroEvento) {

        super(titulo);
        JMenuBar menuBar = criarBarraMenus();
        this.tabelaListaUtilizadores = tabelaListaUtilizadores;
        this.tabelaListaEventos = tabelaListaEventos;
        ficheiroInformacaoBinEvento=ficheiroEvento;
        this.empresa = empresa;
        tabPane = criarSeparadores();
        setJMenuBar(menuBar);

        add(tabPane);
         addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                sair();
            }
    });

//        setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        pack();
        setLocationRelativeTo(null);
        setExtendedState(JFrame.MAXIMIZED_BOTH);
        setVisible(true);

    }

    private JMenuBar criarBarraMenus() {
        JMenuBar menuBar = new JMenuBar();

        menuBar.add(criarMenu());

        return menuBar;
    }

    private JMenu criarMenu() {
        JMenu menu = new JMenu("Menu");
        menu.setMnemonic(KeyEvent.VK_T);

        menu.addSeparator();
        menu.add(criarSubMenuLista());
        
     

        return menu;
    }

    private JMenu criarSubMenuLista() {
        JMenu menu = new JMenu("Lista");
        menu.setMnemonic(KeyEvent.VK_L);

        menu.add(criarItemImportarLista());
        menu.add(criarItemExportarLista());

        personalizarFileChooserEmPortugues();
        fileChooser = new JFileChooser();

        return menu;
    }

    private JTabbedPane criarSeparadores() {
        JTabbedPane tabPane = new JTabbedPane();
        tabPane.addTab("Início", new PainelPrincipal());

        PainelListaUtilizadores pListaUtlizadores = new PainelListaUtilizadores(Janela.this, tabelaListaUtilizadores, empresa);
        tabPane.addTab("Lista Utilizadores", pListaUtlizadores);

        PainelListaEventos pListaEventos = new PainelListaEventos(Janela.this, tabelaListaEventos, empresa);

        tabPane.addTab("Lista Eventos", pListaEventos);
        return tabPane;
    }

    private JMenuItem criarItemImportarLista() {
        JMenuItem item = new JMenuItem("Importar Eventos", KeyEvent.VK_I);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_I, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                fileChooser = new JFileChooser();
                definirFiltroExtensaoBin(fileChooser);

                int resposta = fileChooser.showOpenDialog(Janela.this);

                if (resposta == JFileChooser.APPROVE_OPTION) {
                    FicheiroInformacaoBin ficheiroListaEventos = new FicheiroInformacaoBin();
                    File file = fileChooser.getSelectedFile();
                    ArrayList<Evento> listaEventos = ficheiroListaEventos.ler(file.getPath());
                    if (listaEventos == null) {
                        JOptionPane.showMessageDialog(
                                Janela.this,
                                "Impossível ler o ficheiro: " + file.getPath() + " !",
                                "Importar",
                                JOptionPane.ERROR_MESSAGE);
                    } else {
                        ModeloListaEventos modeloTabela = (ModeloListaEventos) tabelaListaEventos.getModel();
                        int totalEventosAdicionados = 0;
                        for (Evento evento : listaEventos) {
                            modeloTabela.addRow(evento);
                            empresa.addEvento(evento);
                            totalEventosAdicionados++;
                        }
                        JOptionPane.showMessageDialog(
                                Janela.this,
                                "Eventos importados: " + totalEventosAdicionados,
                                "Importar Lista de Eventos",
                                JOptionPane.INFORMATION_MESSAGE);
                    }
                }
            }
        });

        return item;
    }

    private JMenuItem criarItemExportarLista() {
        JMenuItem item = new JMenuItem("Exportar", KeyEvent.VK_X);
        item.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_X, InputEvent.CTRL_MASK));
        item.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {

                definirFiltroExtensaoBin(fileChooser);
                int resposta = fileChooser.showSaveDialog(Janela.this);
                if (resposta == JFileChooser.APPROVE_OPTION) {
                    File file = fileChooser.getSelectedFile();
                    if (!file.getName().endsWith(".bin")) {
                        file = new File(file.getPath().trim() + ".bin");
                    }
                    FicheiroInformacaoBin ficheiroListaEventos = new FicheiroInformacaoBin();
                    boolean ficheiroGuardado
                            = ficheiroListaEventos.guardar(file.getPath(), (ArrayList<Evento>) empresa.getListaEventos());
                    if (!ficheiroGuardado) {
                        JOptionPane.showMessageDialog(
                                Janela.this,
                                "Impossível gravar o ficheiro: "
                                + file.getPath() + " !",
                                "Exportar",
                                JOptionPane.ERROR_MESSAGE);
                    } else {
                        JOptionPane.showMessageDialog(
                                Janela.this,
                                "Ficheiro gravado com sucesso.",
                                "Exportar",
                                JOptionPane.INFORMATION_MESSAGE);
                    }
                }
            }
        });

        return item;
    }
    

    private void definirFiltroExtensaoBin(JFileChooser fileChooser) {
        fileChooser.setFileFilter(new FileFilter() {
            @Override
            public boolean accept(File f) {
                if (f.isDirectory()) {
                    return true;
                }
                String extensao = extensao(f);
                if (extensao != null) {
                    return extensao.equals("bin");
                }
                return false;
            }

            @Override
            public String getDescription() {
                return "*.bin";
            }

            private String extensao(File f) {
                String ext = null;
                String s = f.getName();
                int i = s.lastIndexOf(".");
                if (i != -1) {
                    ext = s.substring(i + 1).toLowerCase();
                }
                return ext;
            }
        });
    }

    private void sair() {
        boolean informacaoGuardada = ficheiroInformacaoBinEvento.guardar(FicheiroInformacaoBin.NOME, (ArrayList<Evento>) empresa.getListaEventos());
        if (!informacaoGuardada) {
            JOptionPane.showMessageDialog(
                    Janela.this,
                    "Impossível guardar informação!",
                    "Sair",
                    JOptionPane.ERROR_MESSAGE);
        }
        dispose();
    }

    public static void personalizarFileChooserEmPortugues() {

        UIManager.put("FileChooser.openDialogTitleText", "Abrir");
        UIManager.put("FileChooser.saveDialogTitleText", "Guardar");

        UIManager.put("FileChooser.acceptAllFileFilterText", "Todos os Ficheiros");

        UIManager.put("FileChooser.lookInLabelMnemonic", "E");
        UIManager.put("FileChooser.lookInLabelText", "Procurar em:");

        UIManager.put("FileChooser.saveInLabelMnemonic", "S");
        UIManager.put("FileChooser.saveInLabelText", "Guardar em:");

        UIManager.put("FileChooser.upFolderToolTipText", "Um nível acima");
        UIManager.put("FileChooser.upFolderAccessibleName", "Um nível acima");

        UIManager.put("FileChooser.homeFolderToolTipText", "Ambiente de Trabalho");
        UIManager.put("FileChooser.homeFolderAccessibleName", "Ambiente de Trabalho");

        UIManager.put("FileChooser.newFolderToolTipText", "Criar nova pasta");
        UIManager.put("FileChooser.newFolderAccessibleName", "Criar nova pasta");

        UIManager.put("FileChooser.listViewButtonToolTipText", "Lista");
        UIManager.put("FileChooser.listViewButtonAccessibleName", "Lista");

        UIManager.put("FileChooser.detailsViewButtonToolTipText", "Detalhes");
        UIManager.put("FileChooser.detailsViewButtonAccessibleName", "Detalhes");

        UIManager.put("FileChooser.fileNameLabelMnemonic", "N");
        UIManager.put("FileChooser.fileNameLabelText", "Nome do ficheiro:");

        UIManager.put("FileChooser.filesOfTypeLabelMnemonic", "A");
        UIManager.put("FileChooser.filesOfTypeLabelText", "Ficheiros do tipo:");

        UIManager.put("FileChooser.fileNameHeaderText", "Nome");
        UIManager.put("FileChooser.fileSizeHeaderText", "Tamanho");
        UIManager.put("FileChooser.fileTypeHeaderText", "Tipo");
        UIManager.put("FileChooser.fileDateHeaderText", "Data");
        UIManager.put("FileChooser.fileAttrHeaderText", "Atributos");

        UIManager.put("FileChooser.cancelButtonText", "Cancelar");
        UIManager.put("FileChooser.cancelButtonMnemonic", "C");
        UIManager.put("FileChooser.cancelButtonToolTipText", "Cancelar");

        UIManager.put("FileChooser.openButtonText", "Abrir");
        UIManager.put("FileChooser.openButtonMnemonic", "A");
        UIManager.put("FileChooser.openButtonToolTipText", "Abrir");

        UIManager.put("FileChooser.saveButtonText", "Guardar");
        UIManager.put("FileChooser.saveButtonToolTipText", "S");
        UIManager.put("FileChooser.saveButtonToolTipText", "Guardar");

        UIManager.put("FileChooser.updateButtonText", "Alterar");
        UIManager.put("FileChooser.updateButtonToolTipText", "A");
        UIManager.put("FileChooser.updateButtonToolTipText", "Alterar");

        UIManager.put("FileChooser.helpButtonText", "Ajuda");
        UIManager.put("FileChooser.helpButtonToolTipText", "A");
        UIManager.put("FileChooser.helpButtonToolTipText", "Ajuda");
    }

}
