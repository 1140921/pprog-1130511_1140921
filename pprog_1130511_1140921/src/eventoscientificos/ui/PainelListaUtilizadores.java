/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;

import eventoscientificos.model.Empresa;
import java.awt.BorderLayout;
import javax.swing.BorderFactory;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;

/**
 *
 * @author 1140921
 */
public class PainelListaUtilizadores extends JPanel {

    private JTable tabelaListaUtilizadores;

    public PainelListaUtilizadores(JFrame jframePai, JTable tabelaListaUtilizadores, Empresa empresa) {
        super(new BorderLayout());

        this.tabelaListaUtilizadores = tabelaListaUtilizadores;
        
        

        JPanel p = criarPainelListaUtilizadores();

        add(p, BorderLayout.CENTER);

    }

    public JTable getTableListaUtilizadores() {
        return tabelaListaUtilizadores;
    }

    private JPanel criarPainelListaUtilizadores() {
        JScrollPane scrollPane = new JScrollPane(tabelaListaUtilizadores);
        JPanel p = new JPanel(new BorderLayout());
        final int MARGEM_SUPERIOR = 10, MARGEM_INFERIOR = 10;
        final int MARGEM_ESQUERDA = 10, MARGEM_DIREITA = 10;
        p.setBorder(BorderFactory.createEmptyBorder(MARGEM_SUPERIOR,
                MARGEM_ESQUERDA,
                MARGEM_INFERIOR,
                MARGEM_DIREITA));
        p.add(scrollPane, BorderLayout.CENTER);
        return p;
    }

}
