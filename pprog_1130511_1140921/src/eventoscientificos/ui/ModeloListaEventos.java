/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.ui;
import eventoscientificos.model.Evento;
import java.util.ArrayList;
import java.util.List;
import javax.swing.table.AbstractTableModel;


/**
 *
 * @author 1140921
 */
public class ModeloListaEventos extends AbstractTableModel {

    private Evento e;
    private List<Evento> listaEventos;

    private static String[] nomesColunas = {"Titulo", "Descrição", "Local", "Data Inicio", "Data Fim"};

    public ModeloListaEventos() {
        this.listaEventos = new ArrayList<>();
    }

    @Override
    public int getRowCount() {
        return this.listaEventos.size();
    }

    @Override
    public int getColumnCount() {
        return ModeloListaEventos.nomesColunas.length;
    }

    @Override
    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    @Override
    public String getColumnName(int colunaIndex) {
        return nomesColunas[colunaIndex];
    }

    @Override
    public Class getColumnClass(int c) {
        return getValueAt(0, c).getClass();
    }

    @Override
    public Object getValueAt(int rowIndex, int columnIndex) {
        String nomeColuna = getColumnName(columnIndex);
        String s;

        if (nomeColuna.equalsIgnoreCase(ModeloListaEventos.nomesColunas[0])) {
            s = this.listaEventos.get(rowIndex).getStrTitulo();
        } else if (nomeColuna.equalsIgnoreCase(ModeloListaEventos.nomesColunas[1])) {
            s = this.listaEventos.get(rowIndex).getStrDescricao();
        } else if (nomeColuna.equalsIgnoreCase(ModeloListaEventos.nomesColunas[2])) {
            s = this.listaEventos.get(rowIndex).getStrLocal();
        } else if (nomeColuna.equalsIgnoreCase(ModeloListaEventos.nomesColunas[3])) {
            s = this.listaEventos.get(rowIndex).getStrDataInicio();
        } else {
            s = this.listaEventos.get(rowIndex).getStrDataFim();
        }

        return s;
    }

    @Override
    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        String nomeColuna = getColumnName(columnIndex);

        if (nomeColuna.equalsIgnoreCase(ModeloListaEventos.nomesColunas[0])) {
            this.listaEventos.get(rowIndex).setTitulo((String) value);
        } else if (nomeColuna.equalsIgnoreCase(ModeloListaEventos.nomesColunas[1])) {
            this.listaEventos.get(rowIndex).setDescricao((String) value);
        } else if (nomeColuna.equalsIgnoreCase(ModeloListaEventos.nomesColunas[2])) {
            this.listaEventos.get(rowIndex).setStrLocal((String) value);
        } else if (nomeColuna.equalsIgnoreCase(ModeloListaEventos.nomesColunas[3])) {
            this.listaEventos.get(rowIndex).setDataInicio((String) value);
        } else {
            this.listaEventos.get(rowIndex).setDataFim((String) value);
        }

        fireTableCellUpdated(rowIndex, columnIndex);
    }

    public void addRow(Evento evento) {
        int rowCount = getRowCount();
        this.listaEventos.add(evento);
        fireTableRowsInserted(rowCount, rowCount);
    }

    public void removeRow(Evento evento) {
        int row = this.listaEventos.indexOf(evento);
        this.listaEventos.remove(evento);
        fireTableRowsDeleted(row, row);
    }

    public Evento[] getArray() {
        return this.listaEventos.toArray(new Evento[this.listaEventos.size()]);
    }

}
