package eventoscientificos.model;

/**
 *
 * @author 1140921
 */
public class Utilizador implements Comparable<Utilizador> {

    /**
     * O nome do Utilizador.
     */
    private String strNome;
    /**
     * O username do Utilizador.
     */
    private String strUsername;
    /**
     * A password do Utilizador.
     */
    private String strPassword;
    /**
     * O e-mail do utilizador.
     */
    private String strEmail;

    /**
     * Constroí uma instância de Utilizaddor com o nome, username, password e
     * e-mail como parametro.
     *
     * @param strNome o nome do Utilizador
     * @param strUsername o usermane do Utilizador
     * @param strPassword a password do Utilizador
     * @param strEmail o e-mail do Utilizador
     */
    public Utilizador(String strNome, String strUsername, String strPassword, String strEmail) {

        setNome(strNome);
        setUsername(strUsername);
        setPassword(strPassword);
        setEmail(strEmail);
    }

    /**
     * Devolve o e-mail do Utilizador.
     *
     * @return o e-mail do utilizador
     */
    public String get_strEmail() {
        return strEmail;
    }

    /**
     * Modifica o nome do Utilizador. Se o nome não tiver sido instânciado ou
     * estiver vazio, não são feitas alterações e é lançada uma excepção.
     *
     * @param strNome novo nome do Utilizador
     */
    public void setNome(String strNome) {
        this.strNome = strNome;
        if (strNome == null || strNome.trim().isEmpty()) {
            throw new IllegalArgumentException("Nome invalido");
        }

    }

    /**
     * Modifica o username do Utilizador. Se o username não tiver sido
     * instânciado ou estiver vazio, não são feitas alterações e é lançada uma
     * excepção.
     *
     * @param strUsername o novo username do Utilizador
     */
    public void setUsername(String strUsername) {

        this.strUsername = strUsername;
        if (strUsername == null || strUsername.trim().isEmpty()) {
            throw new IllegalArgumentException("Username invalido");
        }

    }

    /**
     * Modifica a password do Utilizador. Se a password não tiver sido
     * instânciada ou estiver vazia, não são feitas alterações e é lançada uma
     * excepção.
     *
     * @param strPassword a nova password do Utilizador.
     */
    public void setPassword(String strPassword) {
        this.strPassword = strPassword;

        if (strPassword == null || strPassword.trim().isEmpty()) {
            throw new IllegalArgumentException("Password invalido");
        }

    }

    /**
     * Modifica o e-mail do Utilizador. Para isto, faz a validação do email. O
     * e-mail tem de ser sempre em minúsculas, não acentuado, sem espaços e no
     * meio conter @
     *
     * @param strEmail o novo e-mail do Utilizador
     */
    public void setEmail(String strEmail) {

        String EMAIL_VERIFICACAO = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

        Boolean b = strEmail.matches(EMAIL_VERIFICACAO);
        if (b == false) {
            throw new IllegalArgumentException("Password invalido");
        }

        this.strEmail = strEmail;
    }

    /**
     * Valida retornando 'true'.
     *
     * @return true
     */
    public boolean valida() {
        return true;
    }

    /**
     * Devolve o nome do Utilizador.
     *
     * @return nome do utilizador
     */
    public String getNome() {
        return strNome;
    }

    /**
     * Devolve o username do Utilizador.
     *
     * @return username do utlizador
     */
    public String getUsername() {
        return strUsername;
    }

    /**
     * Descrição textual do Utlizador. String no formato: "Utilizador:\n";
     * "Nome:(nome) "Username: (username) "Password: (password) "Email: (email)
     *
     * @return informação do utilizador
     */
    @Override
    public String toString() {
        String str = "Utilizador:\n";
        str += "\tNome: " + this.strNome + "\n";
        str += "\tUsername: " + this.strUsername + "\n";
        str += "\tPassword: " + this.strPassword + "\n";
        str += "\tEmail: " + this.get_strEmail() + "\n";

        return str;
    }

    /**
     * Compara o Utilizador com o objeto recebido.
     *
     * @param outroObjeto o objeto a comparar com o utilizador.
     * @return true se o objeto recebido representar um objecto equivalente à
     * utilizador. Caso contrário, retorna false.
     */
    @Override
    public boolean equals(Object outroObjeto) {
        if (this == outroObjeto) {
            return true;
        }
        if (this == null || this.getClass() != outroObjeto.getClass()) {
            return false;
        }

        Utilizador outroUsername = (Utilizador) outroObjeto;

        return this.strUsername.equals(outroUsername.strUsername);

    }

    /**
     * Compara os usernames do utilizador e do outro utilizador recebido por
     * parâmetro.
     *
     * @param outroUsername outroUsername
     * @return 0 se existir ja um utilizador com o mesmo numero 
     * existir
     */
    @Override
    public int compareTo(Utilizador outroUsername) {

        int resultado;

        resultado = this.strUsername.compareTo(outroUsername.strUsername);

        return resultado;

    }

}
