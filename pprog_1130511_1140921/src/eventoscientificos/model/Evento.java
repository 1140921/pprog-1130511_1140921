package eventoscientificos.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author 1140921
 */
public class Evento implements Submissivel, Comparable<Evento>, Serializable {

    /**
     * O título do Evento
     */
    private String strTitulo;
    /**
     * A descrição do Evento
     */
    private String strDescricao;
    /**
     * O local do Evento
     */
    private String strLocal;
    /**
     * A data de início do Evento
     */
    private String strDataInicio;
    /**
     * A data final do Evento
     */
    private String strDataFim;
    /**
     * Lista de submissões do Evento
     */
    private List<Submissao> listaSubmissoes;
    /**
     * Lista de sessões temáticas do Evento
     */
    private List<Sessao> listaSessoes;
   

    /**
     * Cria uma instância de Evento com a lista de submissões a lista de sessões
     * por omissão. Cria um novo ArrayList de submissões e uma ArrayList vazia
     * para as sessões.
     */
    public Evento() {

        listaSubmissoes = new ArrayList<Submissao>();
        listaSessoes = new ArrayList<>();
    }

    /**
     * Cria uma instância de Evento recebendo como parametro o titulo, a
     * descrição, o local, a data de incio e a data final do Evento.
     *
     * @param strTitulo o titulo do Evento
     * @param strDescricao a descrição do Evento
     * @param strLocal o local do Evento
     * @param strDataInicio a data de inicio do Evento
     * @param strDataFim a data final do Evento
     */
    public Evento(String strTitulo, String strDescricao, String strLocal, String strDataInicio,
            String strDataFim) {
        setTitulo(strTitulo);
        setDescricao(strDescricao);
        setStrLocal(strLocal);
        setDataInicio(strDataInicio);
        setDataFim(strDataFim);
        listaSubmissoes = new ArrayList<Submissao>();
        listaSessoes = new ArrayList<>();
    }

    /**
     * Modifica o titulo do Evento. Se o titulo não for instânciado ou estiver
     * vazio, o titulo não é modificado e lança uma exceção.
     *
     * @param strTitulo o novo titulo do Evento
     */
    public void setTitulo(String strTitulo) {
        this.strTitulo = strTitulo;
        if (this.strTitulo == null || this.strTitulo.trim().isEmpty()) {
            throw new IllegalArgumentException("Titulo não inserido");
        }

    }

    /**
     * Modifica a descrição do Evento. Se a descrição não for instânciada ou
     * estiver vazia, a descrição não é modificada e lança uma exceção
     *
     * @param strDescricao a nova descrição do Evento
     */
    public void setDescricao(String strDescricao) {
        this.strDescricao = strDescricao;

        if (this.strDescricao == null || this.strDescricao.trim().isEmpty()) {
            throw new IllegalArgumentException("Descricao não  inserida");
        }

    }

    /**
     * Modifica a data de Inicio do Evento.
     *
     * @param strDataInicio String com "dia/mes/ano"
     */
    public void setDataInicio(String strDataInicio) {

        String[] dataInicio;

        dataInicio = strDataInicio.trim().split("/");

        int dia = Integer.parseInt(dataInicio[0]);
        int mes = Integer.parseInt(dataInicio[1]);
        int ano = Integer.parseInt(dataInicio[2]);

        if (mes < 1 || mes > 12) {
            throw new MesInvalidoException("Mês " + mes + " é inválido!!");
        }
        if (dia < 1 || (mes == 2 && Evento.isAnoBissexto(ano)
                ? dia > 29
                : dia > Evento.diasPorMes[mes])) {
            throw new DiaInvalidoException("Dia " + ano + "/" + mes + "/" + dia + " é inválido!!");
        }

        this.strDataInicio = strDataInicio;
    }

    /**
     * Modifica a data final do Evento *
     *
     *
     * @param strDataFim String com "dia/mes/ano"
     */
    public void setDataFim(String strDataFim) {

        String[] dataFim;

        dataFim = strDataInicio.trim().split("/");

        int dia = Integer.parseInt(dataFim[0]);
        int mes = Integer.parseInt(dataFim[1]);
        int ano = Integer.parseInt(dataFim[2]);

        if (mes < 1 || mes > 12) {
            throw new MesInvalidoException("Mês " + mes + " é inválido!!");
        }
        if (dia < 1 || (mes == 2 && Evento.isAnoBissexto(ano)
                ? dia > 29
                : dia > Evento.diasPorMes[mes])) {
            throw new DiaInvalidoException("Dia " + ano + "/" + mes + "/" + dia + " é inválido!!");
        }

        this.strDataFim = strDataFim;
    }

    /**
     * Modifica o local do Evento. Se o local não for instânciado ou estiver
     * vazio, o local não é modificado e lança uma exceção
     *
     * @param strlocal o novo local do Evento
     */
    public void setStrLocal(String strlocal) {

        this.strLocal = strlocal;
        if (strLocal == null || strLocal.trim().isEmpty()) {
            throw new IllegalArgumentException("Local não inserido");
        }

    }

    /**
     * A descrição textual do Evento, uma string no formato: "Titulo : (titulo)
     * Descrição : (descrição) Local :(local) Data inicio :(data de incio) Data
     * fim: (data final)
     *
     * @return as caracteristicas do Evento
     */
    @Override
    public String toString() {
        return "Titulo :" + this.getStrTitulo() + "\nDescrição :" + this.getStrDescricao() + "\nLocal :" + this.getStrLocal() + "\nData inicio :" + getStrDataInicio() + "\nData fim :" + this.getStrDataFim();
    }

    /**
     * Devolve o titulo do Evento
     *
     * @return o titulo do Evento
     */
    public String getStrTitulo() {
        return strTitulo;
    }

    /**
     * Devolve a descrição do Evento
     *
     * @return a descrição do Evento
     */
    public String getStrDescricao() {
        return strDescricao;
    }

    /**
     * Devolve o local do Evento
     *
     * @return o local do Evento
     */
    public String getStrLocal() {
        return strLocal;
    }

    /**
     * Devolve a data de inicio do Evento
     *
     * @return a data de inicio do Evento
     */
    public String getStrDataInicio() {
        return strDataInicio;
    }

    /**
     * Devolve a data final do Evento
     *
     * @return a data final do Evento
     */
    public String getStrDataFim() {
        return strDataFim;
    }

    /**
     * Devolve true se o ano passado por parâmetro for bissexto. Se o ano
     * passado por parâmetro não for bissexto, devolve false.
     *
     * @param ano o ano a validar.
     * @return true se o ano passado por parâmetro for bissexto, caso contrário
     * devolve false.
     */
    public static boolean isAnoBissexto(int ano) {
        return ano % 4 == 0 && ano % 100 != 0 || ano % 400 == 0;
    }

    /**
     * Número de dias de cada mês do ano.
     */
    private static int[] diasPorMes = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30,
        31, 30, 31};

    public Submissao novaSubmissao(Artigo artigo) {
        return new Submissao(artigo);
    }

    /**
     * Adiciona uma nova submissão
     *
     * @param submissao Submisaao
     * @return true se submissao não existir false se ja existir submissão
     */
    public boolean addSubmissao(Submissao submissao) {
        if (validaSubmissao(submissao)) {
            return this.listaSubmissoes.add(submissao);
        } else {
            return false;
        }
    }

    /**
     * Adiciona uma sessão temática à lista de sessões
     *
     * @param sessao do Evento
     * @return true se sessao for adicionada
     */
    public boolean addSessao(Sessao sessao) {
        return this.listaSessoes.add(sessao);
    }

    /**
     * Valida a submissão retornando 'true'
     *
     * @param submissao
     * @return
     */
    private boolean validaSubmissao(Submissao submissao) {
        return submissao.valida();
    }

    public boolean valida() {
        return true;

    }

    /**
     * Compara o Evento com o objeto recebido.
     *
     * @param outroObjeto o objeto a comparar com o Evento.
     * @return true se o objeto recebido representar um Evento equivalente ao
     * Evento. Caso contrário, retorna false.
     */
    @Override
    public boolean equals(Object outroObjeto) {

        if (this == outroObjeto) {
            return true;
        }
        if (this == null || this.getClass() != outroObjeto.getClass()) {
            return false;
        }

        Evento outroEvento = (Evento) outroObjeto;

        return this.strTitulo.equals(outroEvento.strTitulo);

    }

    /**
     * Compara alfabeticamente o titulo do Evento e do Evento recebido por
     * parametro
     *
     * @param outroEvento o evento a ser comparado.
     * @return igual 0 se existir menor0 se n existir
     */
    @Override
    public int compareTo(Evento outroEvento) {

        int resultado;

        resultado = this.strTitulo.compareTo(outroEvento.strTitulo);

        return resultado;

    }

    /**
     * Devolve a lista de submissões do evento
     *
     * @return lista de submissões
     */
    public List<Submissao> getListaSubmissoes() {
        return listaSubmissoes;
    }

    /**
     * Devolve a lista de sessões do Evento
     *
     * @return lista de sessões
     */
    public List<Sessao> getListaSessoes() {
        return listaSessoes;

    }

    /**
     * Aceita submissões. Retorna 'true' quando evocado.
     *
     * @return true
     */
    public boolean aceitaSubmissoes() {
        return true;
    }

}
