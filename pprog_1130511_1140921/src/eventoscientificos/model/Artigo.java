package eventoscientificos.model;

import java.io.Serializable;
import java.util.*;
import java.util.List;

/**
 *
 * @author 1140921
 */
public class Artigo implements Serializable {

    /**
     * O título do Artigo
     */
    private String strTitulo;
    /**
     * O resumo descritivo do Artigo
     */
    private String strResumo;
    /**
     * A lista de Autores do Artigo
     */
    private List<Autor> listaAutores;
    /**
     * O autor corresponde ao Artigo em questão
     */
    private Autor autorCorrespondente;
    /**
     * O nome do ficheiro
     */
    private String strFicheiro;

    /**
     * Titulo por omissão "sem titulo"
     */
    private final String TITULO_POR_OMISSAO = "Sem Titulo";
    /**
     * Resumo por omissao "sem resumo"
     */
    private final String RESUMO_POR_OMISSAO = "Sem resumo";
    /**
     * ficheiro por omissao "sem ficheiro"
     */
    private final String FICHEIRO_POR_OMISSAO = "Sem ficheiro";

    /**
     * Constrói uma instância de Artigo recebendo o titulo, o resumo, a lista de
     * autores, o autor correspondente e o nome do ficheiro
     *
     * @param strTitulo o titulo do Artigo.
     * @param strResumo o resumo do Artigo.
     * @param listaAutores a lista de autores do Artigo.
     * @param autorCorrespondente o autor correspondente do Artigo.
     * @param strFicheiro o nome do ficheiro.
     */
    public Artigo(String strTitulo, String strResumo, List<Autor> listaAutores, Autor autorCorrespondente, String strFicheiro) {

        setTitulo(strTitulo);
        setResumo(strResumo);
        setAutorCorrespondente(autorCorrespondente);
        setFicheiro(strFicheiro);
        setListaAutores(listaAutores);

    }

    /**
     * Constrói uma instância de Artigo com o titulo, o resumo, a lista de
     * autores, o autor correspondente e o nome do ficheiro por omissão.
     */
    public Artigo() {
        setTitulo(TITULO_POR_OMISSAO);
        setResumo(RESUMO_POR_OMISSAO);
        setFicheiro(FICHEIRO_POR_OMISSAO);
        setAutorCorrespondente(new Autor());
        this.listaAutores = new ArrayList<Autor>();

    }

    /**
     * Modifica o título do Artigo
     *
     * @param strTitulo o novo titulo do Artigo
     */
    public void setTitulo(String strTitulo) {
        this.strTitulo = strTitulo;
        if (this.getStrTitulo() == null || this.getStrTitulo().trim().isEmpty()) {
            throw new IllegalArgumentException("Titulo não inserido");
        }

    }

    /**
     * Modifica o resumo do Artigo
     *
     * @param strResumo o novo resumo do Artigo
     */
    public void setResumo(String strResumo) {
        this.strResumo = strResumo;
        if (this.getStrResumo() == null || this.getStrResumo().trim().isEmpty()) {
            throw new IllegalArgumentException("Resumo não inserido");
        }

    }

    /**
     * Modifica a lista de autores
     *
     * @param listaAutores a nova lista de autores
     */
    public void setListaAutores(List<Autor> listaAutores) {
        this.listaAutores = listaAutores;
    }

    /**
     * Modifica o autor correspondente do Artigo
     *
     * @param autor o novo autor correspondente do Artigo
     */
    public void setAutorCorrespondente(Autor autor) {
        this.autorCorrespondente = autor;
    }

    /**
     * Modifica o nome do ficheiro
     *
     * @param strFicheiro o novo nome do ficheiro
     */
    public void setFicheiro(String strFicheiro) {
        this.strFicheiro = strFicheiro;
    }

    /**
     * Cria um novo autor recebendo por parametro o nome e a instituição de
     * afiliação do Autor
     *
     * @param strNome o nome do autor
     * @param strAfiliacao a instituição de afiliação do autor
     * @return o novo autor
     */
    public Autor novoAutor(String strNome, String strAfiliacao) {
        Autor autor = new Autor(strNome, strAfiliacao);

        return autor;
    }

    /**
     * Devolve um ArrayList com os possíveis autores correspondentes do artigo
     *
     * @return os possíveis autores correspondentes
     */
    public List<Autor> getPossiveisAutoresCorrespondentes() {
        List<Autor> la = new ArrayList<Autor>();

        for (Autor autor : this.getListaAutores()) {

            la.add(autor);

        }
        return la;
    }

    public boolean valida() {
        return true;
    }

    /**
     * Descrição textual do Artigo no formato: "Titulo :(titulo) Resumo :
     * (resumo)".
     *
     * @return as informações do Artigo
     */
    @Override
    public String toString() {
        return "Titulo :" + this.getStrTitulo() + "\nResumo :" + this.getStrResumo() + "\nAutores :" + this.mostrarAutores() + "\nAutor Correspondente :" + this.getAutorCorrespondente() + "\nFicheiro" + this.getStrFicheiro();
    }

    /**
     * Obtém o autor recendo por parametro o indice(posição na qual se encontra
     * na lista de autores)
     *
     * @param indice do autor
     * @return o autor
     */
    public Autor obterAutor(int indice) {
        return getListaAutores().get(indice);
    }

    /**
     * Adiciona um novo autor à lista de autores, caso este não se encontre
     * ainda na lista. Se for adicionado retorna 'true', se já existir retorna
     * 'false'.
     *
     * @param autor autor do Artigo
     * @return true se adicionou autor false se autor ja existir
     */
    public boolean adicionarAutor(Autor autor) {
        if (!this.listaAutores.contains(autor)) {
            return this.getListaAutores().add(autor);
        }
        return false;
    }

    /**
     * Remove um autor da lista de autores recendo por parametro um autor
     *
     * @param autor autor do Artigo
     * @return lista de autores sem o autor pretendido
     */
    public boolean removerAutor(Autor autor) {
        return this.getListaAutores().remove(autor);
    }

    /**
     * Devolve o tamanho da lista de autores. Número de autores pertencentes ao
     * artigo.
     *
     * @return tamanho do ArrayList lista de autores
     */
    public int tamanho() {
        return this.getListaAutores().size();
    }

    /**
     * Devolve a posição em que se encontra o Autor no ArrayList lista de
     * autores
     *
     * @param autor autor do Artigo
     * @return indice do autor dentro da lista
     */
    public int indiceDe(Autor autor) {
        return this.getListaAutores().indexOf(autor);
    }

    /**
     * Verifica se o autor existe dentro da lista de autores
     *
     * @param autor autor do Artigo
     * @return true se o autor existir false se o autor n estiver contido na listaAutores
     */
    public boolean contem(Autor autor) {
        return this.getListaAutores().contains(autor);
    }

    /**
     * Adiciona uma nova lista de autores ao Artigo. recebe uma nova lista de
     * autores por parametro, elimina a lista actual e adiciona a nova recebida
     * por parametro.
     *
     * @param listaAutores lista de autores de Artigo
     * @return a nova lista de autores
     */
    public Autor adicionarNovaListaAutores(List<Autor> listaAutores) {
        this.getListaAutores().clear();
        this.getListaAutores().addAll(listaAutores);
        return (Autor) this.getListaAutores();
    }

    /**
     * Obtém o nome do caminho do ficheiro
     *
     * @return the strFicheiro caminho do ficheiro
     */
    public String getStrFicheiro() {
        return strFicheiro;
    }

    /**
     * Obtém o Titulo do Artigo
     *
     * @return the strTitulo String tirulo do artigo
     */
    public String getStrTitulo() {
        return strTitulo;
    }

    /**
     * Obtem o Resumo do Artigo
     *
     * @return the strResumo resumo do Artigo
     */
    public String getStrResumo() {
        return strResumo;
    }

    /**
     * Devolve o autor correspondente do artigo
     *
     * @return o autor correspondente do Artigo
     */
    public Autor getAutorCorrespondente() {
        return autorCorrespondente;
    }

    /**
     * Devolve um ArrayList com as lista de autores do Artigo
     *
     * @return a lista de autores
     */
    public List<Autor> getListaAutores() {
        return listaAutores;
    }

    /**
     * 
     * @return a lista de Autores
     */
    public String mostrarAutores() {

        String listAutores = "";

        for (Autor autor : listaAutores) {
            listAutores += autor + ";";
        }
        return listAutores;

    }

}
