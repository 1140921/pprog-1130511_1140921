package eventoscientificos.model;

import java.io.Serializable;

/**
 *
 * @author 1140921
 */
public class Autor implements Serializable {

    /**
     * O nome do Autor.
     */
    private String strNome;
    /**
     * A instituição de afiliação a que o autor pertence.
     */
    private String strAfiliacao;
    /**
     * Nome por omissão "sem nome"
     */
    private final String NOME_POR_OMISSAO = "sem nome";
    /**
     * Afiliacao por omissao "sem afiliacao"
     */
    private final String AFILIACAO_POR_OMISSAO = "sem afiliacao";

    /**
     * Cria uma instância de Autor recebendo como parametros o nome e a
     * instituição de afiliação.
     *
     * @param strNome o nome do Autor
     * @param strAfiliacao a instituição de afiliação do Autor
     */
    public Autor(String strNome, String strAfiliacao) {
        setNome(strNome);
        setAfiliacao(strAfiliacao);
    }

    /**
     * Cria uma instância de Autor com o nome e a instituição de afiliação por
     * omissão.
     */
    public Autor() {
        setNome(NOME_POR_OMISSAO);
        setAfiliacao(AFILIACAO_POR_OMISSAO);
    }

    /**
     * Modifica o nome do Autor. Se o nome não for instânciado ou estiver vazio,
     * o nome não é modificado e lança uma exceção.
     *
     * @param strNome o novo nome do Autor
     */
    public void setNome(String strNome) {
        this.strNome = strNome;
        if (this.strNome == null || this.strNome.trim().isEmpty()) {
            throw new IllegalArgumentException("Nome não inserido");
        }

    }

    /**
     * Modifica o instituição de Afiliação do Autor. Se a instituição de
     * afiliação não for instânciada ou estiver vazia, o instituição de
     * afiliação não é modificado e lança uma exceção.
     *
     * @param strAfiliacao a nova instituição de afiiação
     */
    public void setAfiliacao(String strAfiliacao) {
        this.strAfiliacao = strAfiliacao;
        if (this.strAfiliacao == null || this.strAfiliacao.trim().isEmpty()) {
            throw new IllegalArgumentException("Instituição Afiliação não inserido");
        }

    }

    /**
     * Devolve o nome do Autor
     *
     * @return o nome doa Autor
     */
    public String getStrNome() {
        return strNome;
    }

    /**
     * Devolve a instuição de afiliação do Autor
     *
     * @return a instituição de Afiliação do Autor
     */
    public String getStrAfiliacao() {
        return strAfiliacao;
    }

    /**
     * Desrição textual do Autor no formato "(nome) - (instituição de afiliação)
     *
     * @return caracteristicas do Autor
     */
    @Override
    public String toString() {
        return "\nNome :"+ this.strNome + "\nAfiliação : " + this.strAfiliacao;
    }
}
