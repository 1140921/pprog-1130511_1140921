package eventoscientificos.model;

import java.util.*;
import java.util.List;
import java.io.Serializable;

/**
 *
 * @author 1140921
 */
public class Empresa implements Serializable {

    /**
     * A lista de utilizadores da Empresa
     */
    private List<Utilizador> listaUtilizadores;
    /**
     * A lista de eventos da Empresa
     */
    private List<Evento> listaEventos;

    /**
     * Controi uma instancia de Empresa com a lista de utilizadores e a lista de
     * eventos por omissão. Cria novas ArrryList vazias para a lisrta de
     * utiloizadores e lista de eventos.
     */
    public Empresa() {
        listaUtilizadores = new ArrayList<Utilizador>();
        listaEventos = new ArrayList<Evento>();

    }

    /**
     * Cria um novo objecto Evento.
     *
     * @return Evento
     */
    public Evento novoEvento() {
        return new Evento();
    }

    /**
     * Adiciona um novo Utilizador à lista de Utiliadores
     *
     * @param u o Utilizador da Empresa
     * @return true se Utilizador for adicionado
     */
    public boolean addUtilizador(Utilizador u) {
        return listaUtilizadores.add(u);
    }

    /**
     * Adiciona um novo Evento à lista de Eventos da Empresa
     *
     * @param e o Evento da Empresa
     * @return true se Evento for adicionado false se ja existir evento
     */
    public boolean addEvento(Evento e) {
        return getListaEventos().add(e);
    }

    /**
     * Devolve a lista de Eventos que podem ser submmetidos. Cria uma nova
     * ArrayList de Eventos, verifica quais os eventos dentro da lista de
     * eventos pode ser submtido e adiciona na ArrayList criada.
     *
     * @return lista de eentos que podem ser submetidos
     */
    public List<Evento> getListaEventosPodeSubmeter() {

        return listaEventos;
    }

    /**
     * Devolve a lista de Utilizadores que podem fazer submmissoes. Cria uma
     * nova ArrayList de Utilizadores, verifica quais os utilizadores dentro da
     * lista de utilizadores .
     *
     * @return lista de utilizadores que podem fazer submissoes
     */
    public List<Utilizador> getListaUtilizadoresRegistados() {

        return listaUtilizadores;
    }

    /**
     * Retorna a lista de eventos existentes na Empresa
     *
     * @return a lista de eventos
     */
    public List<Evento> getListaEventos() {
        return listaEventos;
    }

    /**
     * Retorna a lista de utilizadores existentes na Empresa
     *
     * @return a lista de utilizadores
     */
    public List<Utilizador> getListaUtilizadores() {
        return listaUtilizadores;
    }

}
