/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

/**
 *
 * @author 1140921
 */
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

public class Sessao implements Submissivel,Serializable{

    /**
     * O código da Sessão
     */
    private String codigo;
    /**
     * A descrição da Sessão
     */
    private String descricao;
    /**
     * A lista de submissões da sessão
     */
    private ArrayList<Submissao> listaSubmissoes;

    /**
     * Constroí uma instância de Sessão com o codigo e adescrição como
     * parametros e a lista de submissões por omissão (cria um novo ArrayList).
     *
     * @param codigo codigo da Sessão
     * @param descricao descrição da Sessão
     */
    public Sessao(String codigo, String descricao) {

        this.codigo = codigo;
        this.descricao = descricao;
        this.listaSubmissoes = new ArrayList<>();

    }

    /**
     * Devolve o código da Sessão.
     *
     * @return codigo da sessão
     */
    public String getCodigo() {
        return this.codigo;
    }

    /**
     * Devolve a descrição da Sessão.
     *
     * @return descrição da sessão
     */
    public String getDescricao() {
        return descricao;
    }

    /**
     * Devolve a lista de Submissões da Sessão.
     *
     * @return lista de submissoes
     */
    public List<Submissao> getListaSubmissoes() {
        return listaSubmissoes;
    }

    /**
     * Modifica o código da Sessão.
     *
     * @param codigo codigo da Sessão
     */
    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    /**
     * Modifica a descriçao da Sessão.
     *
     * @param descricao descrição da Sessão
     */
    public void setDescricao(String descricao) {
        this.descricao = descricao;
    }

    /**
     * Valida retornando true
     *
     * @return true
     */
    public boolean valida() {
        return true;
    }

    /**
     * Devolve o código da Sessão na forma de string
     *
     * @return codigo da sessão
     */
    @Override
    public String toString() {
        return "Codigo : "+this.getCodigo() + "\nDescrição : "+this.getDescricao();
    }

    /**
     *
     * @return true
     */
    public boolean aceitaSubmissoes() {
        return true;
    }

    /**
     * Cria uma nova submissão de um Artigo
     *
     * @param artigo artigo
     * @return novo artigo submtido
     */
    public Submissao novaSubmissao(Artigo artigo) {
        return new Submissao(artigo);
    }

    /**
     * Adiciona uma nova submissão à lista de Submissões. Se a submissão for
     * válida, adiciona à lista e retorna 'true', caso contrário, não adiciona e
     * retorna 'false'.
     *
     * @param submissao Submissão
     * @return true se submissao dor adicionada
     */
    public boolean addSubmissao(Submissao submissao) {
        if (validaSubmissao(submissao)) {
            return this.getListaSubmissoes().add(submissao);
        } else {
            return false;
        }
    }

    /**
     * Valida submissão
     *
     * @param submissao Submissão
     * @return true
     */
    private boolean validaSubmissao(Submissao submissao) {
        return submissao.valida();
    }

}
