package eventoscientificos.model;

import java.io.Serializable;

/**
 *
 * @author 1140921
 */
public class Submissao implements Serializable {

    /**
     * O Artigo da Submissão
     */
    private Artigo artigo;

    /**
     * Cria uma instância de Submissão com o artigo como parametro.
     *
     * @param artigo Artigo
     */
    public Submissao(Artigo artigo) {
        this.artigo = artigo;
    }

    /**
     * Cria um novo artigo.
     *
     * @return o novo artigo
     */
    public Artigo novoArtigo() {
        return new Artigo();
    }

    /**
     * Devolve a informação textual da Submissão.
     *
     * @return informação textual
     */
    public String getInfo() {
        return this.toString();
    }

    /**
     * Modifica o Artigo a Submeter
     *
     * @param artigo Artigo
     */
    public void setArtigo(Artigo artigo) {
        this.artigo = artigo;
    }

    /**
     * Valida retornando 'true'.
     *
     * @return true valor valido
     */
    public boolean valida() {
        return true;
    }

    /**
     * Descrição textual da Submissão.
     *
     * @return descrição textual
     */
    @Override
    public String toString() {
        return "Submissão:\n"+this.getArtigo();
    }

    /**
     * Devolve o Artigo submetido
     * @return the artigo submetido
     */
    public Artigo getArtigo() {
        return artigo;
    }
    
    
    
    
}
