/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package eventoscientificos.model;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;

/**
 *
 * @author 1140921
 */
public class FicheiroInformacaoBin {

    public static final String NOME = "ListaEventos.bin";

    public FicheiroInformacaoBin() {
    }

    public ArrayList<Evento> ler(String nomeFicheiro) {
        ArrayList<Evento> listaEventos = new ArrayList<>();
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(nomeFicheiro));
            try {
                listaEventos = (ArrayList<Evento>) in.readObject();
            } finally {
                in.close();
            }
            return listaEventos;
        } catch (IOException | ClassNotFoundException ex) {
            return null;
        }
    }

    public boolean guardar(String nomeFicheiro, ArrayList<Evento> listaEventos) {
        try {
            ObjectOutputStream out = new ObjectOutputStream(
                    new FileOutputStream(nomeFicheiro));
            try {
                out.writeObject(listaEventos);
            } finally {
                out.close();
            }
            return true;
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }
    }
}
