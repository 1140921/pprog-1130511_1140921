package eventoscientificos.model;

import java.io.*;
import java.util.*;
import java.util.List;

/**
 *
 * @author 1140921
 */
public class FicheiroAInstanciarTxt {

    
    /**
     * ler ficheiro txt e instancia eventos e sessoes atraves da informacao de um ficheiro de texto
     * @param FILE_TXT fileTxt
     * @return listaEventos
     * @throws FileNotFoundException fileNotException
     */
    public static ArrayList<Evento> lerFicheiroTxtEventos(String FILE_TXT) throws FileNotFoundException {
        ArrayList<Evento> listaEvento = new ArrayList<>();
       
        Scanner fInput = new Scanner(new File(FILE_TXT), "UTF-8");

        while (fInput.hasNextLine()) {
            String s = fInput.nextLine();

            if (s.length() != 0) {
                String[] str = s.trim().split(",");

                if (str[0].equalsIgnoreCase("Evento")) {
                    listaEvento.add(lerEvento(str));
                } else {
                    for (Evento i : listaEvento) {
                        if (i.getStrTitulo().equalsIgnoreCase(str[1])) {
                            i.addSessao(lerSessao(str));
                            break;
                        }
                    }
                }
            }
        }
        fInput.close();

        return listaEvento;
    }

    
    /**
     * ler ficheiro txt e instancia Utilizadores atraves da informacao de um ficheiro de texto
     * @param FILE_TXT filetxt
     * @return listaUtilizadores
     * @throws FileNotFoundException fileNotException
     */
    public static ArrayList<Utilizador> lerFicheiroTxtUtilizador(String FILE_TXT) throws FileNotFoundException {

        ArrayList<Utilizador> listaUtilizador = new ArrayList<>();
        Scanner fInput = new Scanner(new File(FILE_TXT), "UTF-8");

        while (fInput.hasNextLine()) {
            String s = fInput.nextLine();

            if (s.length() != 0) {
                String[] str = s.trim().split(",");

                if (str[0].equalsIgnoreCase("Utilizador")) {

                    listaUtilizador.add(lerUtilizador(str));
                }
            }
        }
        fInput.close();

        return listaUtilizador;
    }

    /**
 * instancia evento atraves de uma linha do ficheiro txt
 * @param str array string
 * @return evento Evento 
 */
    
    public static Evento lerEvento(String str[]) {
        String titulo = str[1];
        String descricao = str[2];
        String local = str[3];
        String dataInicio = str[4];
        String dataFim = str[5];

        Evento e = new Evento(titulo, descricao, local, dataInicio, dataFim);
        return e;

    }
        /**
 * instancia utilizador atraves de uma linha do ficheiro txt
 * @param str array string
 * @return Utilizador utilizador 
 */

    public static Utilizador lerUtilizador(String str[]) {
        String strNome = str[1];
        String strUsername = str[2];
        String strPassword = str[3];
        String strEmail = str[4];

        Utilizador u = new Utilizador(strNome, strUsername, strPassword, strEmail);

        return u;

    }
   /**
     * instancia sessao atraves de umalinha do ficheiro txt
     * @param str array String
     * @return sessao Sessao
     */
    public static Sessao lerSessao(String[] str) {
        String codigo = str[2];
        String descricao = str[3];
        return new Sessao(codigo, descricao);

    }

}
