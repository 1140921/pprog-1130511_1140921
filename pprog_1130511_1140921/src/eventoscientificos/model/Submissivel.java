package eventoscientificos.model;

import java.util.List;

/**
 *
 * @author 1140921
 */
public interface Submissivel {
    
    public abstract Submissao novaSubmissao(Artigo artigo);
    public abstract boolean addSubmissao(Submissao submissao);
   public abstract List<Submissao> getListaSubmissoes();
    
}
