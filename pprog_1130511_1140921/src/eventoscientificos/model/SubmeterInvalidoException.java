package eventoscientificos.model;

/**
 * Representa uma exceção para ser lançada quando uma aplicação tenta
 * criar/modificar uma instância de Data para representar um dia cujo mês é
 * inválido.
 *
 * @author 1140921
 */
public class SubmeterInvalidoException extends IllegalArgumentException {

    /**
     * Constroí um SubmeterInvalidoException com a mensagem "Nenhum evento
     * selecionado".
     */
    public SubmeterInvalidoException() {
        super("Nenhum evento selecionado");
    }

    /**
     * Constroi um UsernameInvalidoException com a mensagem mensagem recebida
     *
     * @param mensagem a mensagem transmitida pela exceção
     */
    public SubmeterInvalidoException(String mensagem) {
        super(mensagem);

    }

}
